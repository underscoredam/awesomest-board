export const toggleDrawerAction = () => {
  return {
    type: 'TOGGLE_DRAWER',
  }

};

export const closeDrawerAction = () => {
  return {
    type: 'CLOSE_DRAWER'
  }
};
