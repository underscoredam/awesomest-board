const tilesData = [
    {
    img: 'https://img.clipartfest.com/475ce5a44bd1ff0b8da65ce9ed7d4aad_art-brush-clipart-1-clipart-artist-brush_197-130.png',
    title: 'Breakfast',
    author: 'jill111',
  },
  {
    img: 'https://img.clipartfest.com/3de0a1e987078433d0f32d28ded47037_-paint-brush-clip-art-free-clipart-artist-brush_900-636.png',
    title: 'Tasty burger',
    author: 'pashminu',
  },
  {
    img: 'https://img.clipartfest.com/e737943c6236c20b2b70761b466f2277_paintbrush-clip-art-clipart-artist-brush_758-1055.png',
    title: 'Camera',
    author: 'Danson67',
  },
  {
    img: 'https://img.clipartfest.com/d74d4943c2eae21a1527ed67db777fb4_-paint-brush-clip-art-free-clipart-artist-brush_341-425.jpeg',
    title: 'Morning',
    author: 'fancycrave1',
  },
  {
    img: 'https://img.clipartfest.com/70d721f58f1fff931b339d941e36cf9a_paintbrush-clipart-2-clipart-artist-brush_830-1330.png',
    title: 'Hats',
    author: 'Hans',
  },
  {
    img: 'https://img.clipartfest.com/a8fe0dd9a8508d13c10de0f276272eb5_paintbrush-clip-art-clipart-artist-brush_255-187.jpeg',
    title: 'Honey',
    author: 'fancycravel',
  },
  {
    img: 'https://img.clipartfest.com/475ce5a44bd1ff0b8da65ce9ed7d4aad_art-brush-clipart-1-clipart-artist-brush_197-130.png',
    title: 'Vegetables',
    author: 'jill111',
  },
];

export default tilesData;
